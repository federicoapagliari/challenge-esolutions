import { Box, Typography } from "@mui/material";
import { StyledDetailInfoContainer } from "./detail.styles";

import { DetailInfoProps } from "./detail.types";
import { StyledButton } from "../../../components/Button/button.styles";

export const DetailInfo = (props: DetailInfoProps) => {
  const { product } = props;

  const discountedPrice =
    product.price - product.price * (product.discountPercentage / 100);
  const formattedPrice = `$${product.price.toFixed(2)}`;

  return (
    <StyledDetailInfoContainer>
      <Box className="first-item border-shadow">
        <Box className="flex-item">
          <Typography className="info-title" variant="h4">
            {product.title}
          </Typography>
          <Typography className="info-subtitle">{product.brand}</Typography>
        </Box>

        <Box className="flex-item">
          <Typography>Stock disponible: {product.stock}</Typography>
        </Box>
      </Box>
      <Typography className="price-discounted info-title">
        {`$${discountedPrice.toFixed(2)}`}
      </Typography>
      <Typography className="price-old border-shadow">
        {formattedPrice}
      </Typography>
      <Typography className="border-shadow">{product.description}</Typography>

      <StyledButton>Add To Cart</StyledButton>
    </StyledDetailInfoContainer>
  );
};
