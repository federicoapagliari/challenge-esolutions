import { useParams } from "react-router-dom";

import { useGetProductByIdQuery } from "../../../app/services/products/products";
import { StyledDetailContainer } from "./detail.styles";

import { DetailImage } from "./DetailImage";
import { DetailInfo } from "./DetailInfo";

export const ProductDetail = () => {
  const { id } = useParams();
  const { data: productData } = useGetProductByIdQuery({ id: id! });

  return (
    <StyledDetailContainer>
      {productData && (
        <>
          <DetailImage image={productData.thumbnail} />
          <DetailInfo product={productData} />
        </>
      )}
    </StyledDetailContainer>
  );
};
