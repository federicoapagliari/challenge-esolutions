type QueryURL = Record<string, string>;

export const QUERY_URL: QueryURL = {
  products: "/products",
};
