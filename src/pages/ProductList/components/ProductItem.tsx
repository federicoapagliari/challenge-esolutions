import {
  CardActions,
  Button,
  Typography,
  IconButton,
  Link,
} from "@mui/material";

import { ProductItemProps } from "./products.type";
import {
  StyledCardContentWrapper,
  StyledCard,
  StyledCardMedia,
  StyledProductItemContainer,
} from "./list.styles";

export const ProductItem = ({ product }: ProductItemProps) => {
  return (
    <StyledProductItemContainer>
      <StyledCard>
        <StyledCardMedia image={product.thumbnail} />
        <StyledCardContentWrapper>
          <Typography gutterBottom variant="h5" component="div">
            {product.title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {product.description}
          </Typography>
        </StyledCardContentWrapper>
        <CardActions>
          <Link href={`/product/${product.id}`}>
            <IconButton color="primary" size="small">
              <Button size="small">See more</Button>
            </IconButton>
          </Link>
        </CardActions>
      </StyledCard>
    </StyledProductItemContainer>
  );
};
