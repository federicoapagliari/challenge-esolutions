import { StyledDetailImageContainer } from "./detail.styles";
import { DetailImageProps } from "./detail.types";

export const DetailImage = (props: DetailImageProps) => {
  const { image } = props;
  return (
    <StyledDetailImageContainer>
      <img className="styled-image" src={image} />
    </StyledDetailImageContainer>
  );
};
