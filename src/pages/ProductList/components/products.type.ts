import { Product } from "../../../app/services/products/products.type";

export interface ProductItemProps {
  product: Product;
}
