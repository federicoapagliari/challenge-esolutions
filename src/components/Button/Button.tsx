import { StyledButton } from "./button.styles";
import { ButtonProps } from "./button.types";

export const Button = (props: ButtonProps) => {
  const { children } = props;
  return <StyledButton>{children}</StyledButton>;
};
