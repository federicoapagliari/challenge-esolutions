import { createBrowserRouter, Navigate } from "react-router-dom";

import App from "./App";

import { ProductsPage } from "./pages/ProductList/ProductsPage";
import { ProductDetailPage } from "./pages/ProductDetail/ProductDetailPage";
import * as URL from "./utils/url";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      { index: true, element: <Navigate to={URL.PRODUCTS} /> },
      {
        path: URL.PRODUCTS,
        element: <ProductsPage />,
      },
      {
        path: URL.PRODUCT,
        element: <ProductDetailPage />,
      },
    ],
  },
]);
