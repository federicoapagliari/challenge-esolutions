import { createTheme } from "@mui/material/styles";

const theme = createTheme({
  typography: {
    fontFamily: ["Satoshi"].join(","),
  },
});

export default theme;
