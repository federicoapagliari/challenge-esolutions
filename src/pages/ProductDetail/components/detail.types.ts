import { Product } from "../../../app/services/products/products.type";

export type DetailImageProps = {
  image: string;
};

export type DetailInfoProps = {
  product: Product;
};
