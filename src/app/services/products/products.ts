import { baseApi } from "../../api/baseApi";
import {
  createGetItemQuery,
  createProvidesTags,
} from "../../utils/creatorUrlTag";
import { QUERY_URL } from "../URL.ts";

import { Product, ProductResponse, TAG_TYPES } from "./products.type";

export const productsApi = baseApi
  .enhanceEndpoints({ addTagTypes: [TAG_TYPES.products] })
  .injectEndpoints({
    endpoints: (builder) => ({
      getProducts: builder.query<ProductResponse, void>({
        query: () => ({ url: QUERY_URL.products, method: "GET" }),
        providesTags: (result) =>
          createProvidesTags<ProductResponse>(result, TAG_TYPES.products) as [],
      }),
      getProductById: builder.query<Product, { id: string }>({
        query: (params) => createGetItemQuery(QUERY_URL.products, params),
        providesTags: [TAG_TYPES.products],
      }),
    }),
  });

export const { useGetProductsQuery, useGetProductByIdQuery } = productsApi;
