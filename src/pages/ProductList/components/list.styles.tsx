import { Card, CardContent, CardMedia, Grid } from "@mui/material";
import { styled } from "styled-components";

export const StyledProductListContainer = styled(Grid)(() => ({
  marginTop: "20px",
  marginLeft: "140px",
  marginRight: "140px",
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "flex-start",
  alignItems: "flex-start",
}));

export const StyledProductItemContainer = styled(Grid)(() => ({
  flex: "0 0 calc(20% - 2vw)",
  margin: "10px",
}));

export const StyledCard = styled(Card)(() => ({
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
  height: "25rem",
}));

export const StyledCardMedia = styled(CardMedia)(() => ({
  paddingTop: "56.25%",
}));

export const StyledCardContentWrapper = styled(CardContent)(() => ({
  flexGrow: 1,
}));
