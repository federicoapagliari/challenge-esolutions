import { styled } from "styled-components";
import { Toolbar } from "@mui/material";

export const StyledToolbar = styled(Toolbar)(() => ({
  backgroundColor: "white",
  ".brand": {
    color: "black",
  },

  ".demo": {
    display: "flex",
    justifyContent: "flex-end",
    color: "black",
  },

  ".styled-link": {
    textDecoration: "none",
  },

  ".nav-item": {
    color: "black",
  },
}));
