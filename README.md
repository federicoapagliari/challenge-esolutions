# React + Vite + RTK Query + MUI

Sigue estos pasos para configurar y ejecutar el proyecto:

1. Clona este repositorio en tu máquina local.
2. Abre una terminal en el directorio raíz del proyecto.
3. Ejecuta el siguiente comando para instalar las dependencias:`npm install`
4. Una vez que todas las dependencias estén instaladas, puedes iniciar la aplicación con el siguiente comando:`npm run dev`
