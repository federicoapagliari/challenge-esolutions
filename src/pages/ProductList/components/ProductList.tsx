import { useGetProductsQuery } from "../../../app/services/products/products";
import { ProductItem } from "./ProductItem";

import { StyledProductListContainer } from "./list.styles";

export const ProductList = () => {
  const { data: productsData, isLoading } = useGetProductsQuery();

  return (
    <StyledProductListContainer>
      {isLoading && <h1>Loading...</h1>}
      {productsData &&
        productsData?.products.map((product) => (
          <ProductItem key={product.id} product={product}></ProductItem>
        ))}
    </StyledProductListContainer>
  );
};
