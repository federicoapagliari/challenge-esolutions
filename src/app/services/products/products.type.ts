export enum TAG_TYPES {
  products = "Products",
}

export interface Product {
  brand: string;
  category: string;
  description: string;
  discountPercentage: number;
  id: number;
  images: string[];
  price: number;
  rating: number;
  stock: number;
  thumbnail: string;
  title: string;
}

export interface ProductResponse {
  total: number;
  skip: number;
  limit: number;
  products: Product[];
}
