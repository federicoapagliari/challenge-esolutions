import { Link } from "react-router-dom";
import {
  AppBar,
  Box,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
} from "@mui/material";

import * as URL from "../../utils/url";

import { StyledToolbar } from "./navbar.styles";

export const Navbar = () => {
  return (
    <AppBar position="static">
      <StyledToolbar>
        <Box className="brand">LOGO</Box>
        <Box>
          <List>
            <ListItem>
              <ListItemButton className="styled-btn">
                <Link className="styled-link" href={URL.PRODUCTS}>
                  <ListItemText className="nav-item" primary="Productos" />
                </Link>
              </ListItemButton>
            </ListItem>
          </List>
        </Box>
      </StyledToolbar>
    </AppBar>
  );
};
