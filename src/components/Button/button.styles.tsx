import { styled } from "styled-components";
import { Button } from "@mui/material";

export const StyledButton = styled(Button)(() => ({
  backgroundColor: "#5467a8",
  color: "#FFFFFF",
  height: "60px",
  width: "250px",
  borderRadius: "30px",
  "&:hover": {
    backgroundColor: "rgb(132, 148, 202)",
  },
}));
