import { styled } from "styled-components";

export const StyledDetailContainer = styled.div(() => ({
  display: "flex",
  gap: "4rem",
  width: "100%",
  padding: "2rem 20rem",
}));

export const StyledDetailInfoContainer = styled.div(() => ({
  flex: 1,

  ".info-title": {
    fontWeight: "500",
  },

  ".info-subtitle": {
    color: "#B9BBBF",
  },

  ".border-shadow": {
    borderBottom: "1px solid #ececec",
    marginBottom: "20px",
    paddingBottom: "20px",
  },

  ".border-shadow:last-child": {
    borderBottom: "none",
  },

  ".first-item": {
    display: "flex",
    gap: "4rem",
  },

  ".flex-item": {
    flex: 1,
  },

  ".price-discounted": {
    color: "#3A4980",
    fontSize: "30px",
  },

  ".price-old": {
    textDecoration: "line-through",
    color: "#B9BBBF",
  },
}));

export const StyledDetailImageContainer = styled.div(() => ({
  flex: 1,

  ".styled-image": {
    borderRadius: "10px",
    boxShadow: "0px 4px 8px rgba(0, 0, 0, 0.2)",
  },
}));
